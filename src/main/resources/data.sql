-- Some sample data.
-- For all users: name == username && username == password.
INSERT INTO public.users (id, name, password, username) VALUES (nextval('userid_sequence'), 'test1', '$2a$10$PXUtkB6Jb9j9RjPAtyz.k.twQtDG5Mg0b3.slRlh0PoNsQ.blY/Yi', 'test1') on conflict do nothing;
INSERT INTO public.users (id, name, password, username) VALUES (nextval('userid_sequence'), 'test2', '$2a$10$SJkloHK8k/tqNzltEk.9SewL/vJds6gd8lg6a6l2N0GvCgkYIeFQm', 'test2') on conflict do nothing;
INSERT INTO public.users (id, name, password, username) VALUES (nextval('userid_sequence'), 'test3', '$2a$10$OnXGz4.XBa0/KB2mmXt52Ogc7SspPkWghffzblEuIPXC74Sk5gns6', 'test3') on conflict do nothing;
INSERT INTO public.users (id, name, password, username) VALUES (nextval('userid_sequence'), 'test4', '$2a$10$T39WIH9H8poP3ZMREI26/eLnLrR/oem2pqgB8wxvjmV.kUXyQs38a', 'test4') on conflict do nothing;
INSERT INTO public.users (id, name, password, username) VALUES (nextval('userid_sequence'), 'test5', '$2a$10$Yr4ACfeWKW2F/AosJD4KgOGaT3ESY7ve1WUP7UNWWLIzrCfVJgGyS', 'test5') on conflict do nothing;
