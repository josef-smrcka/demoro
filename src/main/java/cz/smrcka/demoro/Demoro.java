package cz.smrcka.demoro;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Demoro {
  private static final String SPRING_CONFIG_NAME_PROPERTY = "spring.config.name:config";

  public static void main(String[] args) {
    new SpringApplicationBuilder(Demoro.class).properties(SPRING_CONFIG_NAME_PROPERTY).build().run(args);
  }
}
