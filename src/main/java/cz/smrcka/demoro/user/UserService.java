package cz.smrcka.demoro.user;

import java.util.List;

public interface UserService {
  List<User> getAllUsers();

  User getUserById(Long id);

  User getUserByUsername(String username);

  User saveUser(User user);

  void deleteUser(Long id);
}
