package cz.smrcka.demoro.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "userid_sequence")
  private Long id;
  @Column(length = 255, nullable = false)
  @NotNull(message = "Provide a name.")
  @Size(min = 1, max = 255, message = "Provide a name with length from 1 to 255.")
  private String name;
  // TODO: validate unique username
  @Column(nullable = false, unique = true, length = 255)
  @NotNull(message = "Provide a username.")
  @Size(min = 1, max = 255, message = "Provide a username with length from 1 to 255.")
  private String username;
  @Column(nullable = false, length = 60)
  @NotEmpty(message = "Provide a password.")
  private String password;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
