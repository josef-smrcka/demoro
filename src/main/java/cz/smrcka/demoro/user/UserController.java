package cz.smrcka.demoro.user;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserController {
  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping(value = "/users", produces = "application/json")
  public ResponseEntity<List<User>> getUsers() {
    return ResponseEntity.ok(userService.getAllUsers());
  }

  @GetMapping(value = "/user/{id}", produces = "application/json")
  public ResponseEntity<User> getUserById(@PathVariable Long id) {
    final User user = userService.getUserById(id);
    if (user == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(user);
  }

  @PostMapping(value = "/user", consumes = "application/json")
  public ResponseEntity<?> addUser(@Valid @RequestBody User user) {
    final User savedUser = userService.saveUser(user);
    final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId()).toUri();
    return ResponseEntity.created(uri).build();
  }

  @PutMapping(value = "/user/{id}", consumes = "application/json")
  public ResponseEntity<?> updateUser(
      @Valid @RequestBody User user,
      @PathVariable Long id
  ) {
    final User savedUser = userService.getUserById(id);
    if (savedUser == null) {
      return ResponseEntity.notFound().build();
    }
    user.setId(id);
    userService.saveUser(user);
    return ResponseEntity.noContent().build();
  }
}
