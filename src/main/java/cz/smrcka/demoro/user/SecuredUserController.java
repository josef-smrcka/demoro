package cz.smrcka.demoro.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secured")
public class SecuredUserController {
  private final UserService userService;

  @Autowired
  public SecuredUserController(UserService userService) {
    this.userService = userService;
  }

  @DeleteMapping(value = "/user/{id}")
  public ResponseEntity<Object> deleteUser(@PathVariable Long id) {
    final User user = userService.getUserById(id);
    if (user == null) {
      return ResponseEntity.notFound().build();
    }
    userService.deleteUser(id);
    return ResponseEntity.ok().build();
  }
}
