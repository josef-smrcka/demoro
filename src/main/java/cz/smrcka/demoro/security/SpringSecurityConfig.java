package cz.smrcka.demoro.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
  private final DbUserDetailsService dbUserDetailsService;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired
  public SpringSecurityConfig(
      DbUserDetailsService dbUserDetailsService,
      BCryptPasswordEncoder bCryptPasswordEncoder
  ) {
    this.dbUserDetailsService = dbUserDetailsService;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    // TODO: combine UserDetailsService with default authentication method (username and password in config.properties)
    auth.userDetailsService(dbUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .httpBasic()
        .and()
        .authorizeRequests()
        .antMatchers("/secured/**")
        .authenticated()
        .and()
        .csrf()
        .disable()
        .formLogin()
        .disable();
  }
}
